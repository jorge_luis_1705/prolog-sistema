export const sarampion = [
  "cubierto de puntos",
  "temperatura alta",
  "ojos rojos",
  "tos seca",
];

export const influenza = [
  "dolor en las articulaciones",
  "estornudo",
  "dolor de cabeza",
];
export const malaria = [
  "temperatura alta",
  "dolor en las articulaciones",
  "temblores",
  "escalofrios",
];
export const gripe = ["cuerpo cortado", "dolor de cabeza", "temperatura alta"];

export const covid19 = [
  "fiebre",
  "tos seca",
  "perdida de olfato",
  "cansancio",
  "dolores musculares",
];
export const tifoidea = [
  "falta de apetito",
  "temperatura alta",
  "dolor abdominal",
  "dolor de cabeza",
  "diarrea",
];
