import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

import "./App.css";
import * as pl from "tau-prolog";
import { Fragment, useState } from "react";
import {
  Checkbox,
  FormControlLabel,
  FormGroup,
  Button,
  FormLabel,
  Box,
  TextField,
  Container,
} from "@mui/material";
import { labelList } from "./util/label-sintomas";
import {
  covid19,
  gripe,
  influenza,
  malaria,
  sarampion,
  tifoidea,
} from "./util/sintomas";

const MySwal = withReactContent(Swal);

function App() {
  const [state, setState] = useState("");
  const [consulta, setConsulta] = useState({
    sintomas: [""],
  });

  let sintomasSarampionArry = "";
  sarampion.sort().forEach((e) => (sintomasSarampionArry += `'${e}'`));
  let sintomasInfluenzaArry = "";
  influenza.sort().forEach((e) => (sintomasInfluenzaArry += `'${e}'`));
  let sintomasMalariaArray = "";
  malaria.sort().forEach((e) => (sintomasMalariaArray += `'${e}'`));
  let sintomasGripeArray = "";
  gripe.sort().forEach((e) => (sintomasGripeArray += `'${e}'`));

  let sintomasCovid19Array = "";
  covid19.sort().forEach((e) => (sintomasCovid19Array += `'${e}'`));

  let sintomasTifoideaArray = "";
  tifoidea.sort().forEach((e) => (sintomasTifoideaArray += `'${e}'`));

  const session = pl.create();
  session.consult(
    `conocimiento('sarampion', [${sintomasSarampionArry
      .split("''")
      .join(
        "','"
      )}], ['el paciente esta cubierto de puntos', 'el paciente tiene temperatura alta',
      'el paciente tiene ojos rojos','el paciente tiene tos seca']).
    
    conocimiento('influenza',[${sintomasInfluenzaArry.split("''").join("','")}],
      ['el paciente tiene dolor en las articulaciones', 'el paciente tiene mucho
      estornudo','el paciente tiene dolor de cabeza']).
    
    conocimiento('malaria',[${sintomasMalariaArray.split("''").join("','")}],
    ['el paciente tiene temperatura alta','el paciente tiene dolor en las
    articulaciones', 'el paciente tiembla violentamente', 'el paciente tiene
    escalofrios']).
    
    conocimiento('gripe',[${sintomasGripeArray.split("''").join("','")}],
    ['el paciente tiene cuerpo cortado', 'el paciente tiene dolor de cabeza', 'el
    paciente tiene temparatura alta']).
    
    conocimiento('tifoidea',
    [${sintomasTifoideaArray
      .split("''")
      .join(
        "','"
      )}], ['el paciente tiene falta de apetito', 'el paciente tiene temperatura alta', 'el paciente tiene dolor abdominal', 'el paciente tiene dolor de cabeza', 'el paciente tiene diarrea']).

    conocimiento('covid19',[${sintomasCovid19Array.split("''").join("','")}],
    ['el paciente presenta fiebre', 'el paciente presenta tos seca', 'el paciente presenta perdida de olfato', 'el paciente presenta mucho cansancio', 'el paciente tiene dolores musculares']).
    

    `,
    {
      success: function () {
        /* Program loaded correctly */
      },
      error: function (err) {
        /* Error parsing program */
        console.log({ err });
      },
    }
  );
  const addSitoma = () => {
    setConsulta({
      ...consulta,
      sintomas: [...consulta.sintomas, ""],
    });
  };
  return (
    <Container>
      <Box
        sx={{
          "& > :not(style)": { m: 1, width: "25ch" },
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <TextField
          hiddenLabel
          id="filled-hidden-label-small"
          variant="filled"
          size="small"
          type="text"
          sx={{ bgcolor: "#757de8" }}
          onKeyPress={(e) => {
            if (e.code === "Enter") {
              session.query(`conocimiento(${e.target.value.trim()}, X, Y).`, {
                success: function (goal) {
                  /* Goal loaded correctly */
                  session.answer({
                    success: function (answer) {
                      setState(session.format_answer(answer));
                      let res = session
                        .format_answer(answer)
                        .split("Y =")[1]
                        .trim()
                        .replace("[", "")
                        .replace("]", "")
                        .replace(".", "")
                        .split(",");
                      const arr = res;
                      MySwal.fire({
                        icon: "warning",
                        title: JSON.stringify(e.target.value.trim()),
                        text: JSON.stringify(arr),
                      });
                    },
                    fail: function () {
                      setState("error");
                      /* No more answers */
                    },
                    error: function (err) {
                      /* Uncaught exception */
                    },
                    limit: function () {
                      /* Limit exceeded */
                    },
                  });
                },
                error: function (err) {
                  /* Error parsing goal */
                },
              });
            }
          }}
        />
        <Button
          variant="contained"
          sx={{ bgcolor: "#3f51b5" }}
          onClick={addSitoma}
        >
          Añadir Sintoma
        </Button>
      </Box>

      {consulta.sintomas.map((e, index) => (
        <Fragment key={e.id}>
          <FormLabel
            sx={{
              bgcolor: "#ff7961",
              fontSize: "20px",
              borderRadius: "4px",
              textAlign: "center",
              fontWeight: "600",
            }}
            component="legend"
          >
            Sintoma{" " + (index + 1)}
          </FormLabel>
          <FormGroup
            component="fieldset"
            variant="standard"
            aria-label="position"
            row
          >
            {labelList.map((element) => (
              <Box
                sx={{
                  "& > :not(style)": { m: 1, width: "25ch" },
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "center",
                }}
                key={element.id}
              >
                <FormControlLabel
                  sx={{
                    m: 0.5,
                    p: 0.5,
                    borderRadius: "4px",
                    bgcolor: "#90caf9",
                    fontWeight: "800",
                  }}
                  control={
                    <Checkbox
                      disabled={
                        consulta.sintomas.toString().includes(element.sintoma)
                          ? true
                          : false
                      }
                      defaultChecked={false}
                      value={element.sintoma}
                    />
                  }
                  disabled={e[index] ? true : false}
                  label={element.sintoma}
                  labelPlacement="top"
                  onChange={(event, checked) => {
                    setConsulta({
                      ...consulta,
                      sintomas: consulta.sintomas.map((el, i) =>
                        i === index ? (el = event.currentTarget.value) : el
                      ),
                    });
                  }}
                />
              </Box>
            ))}
          </FormGroup>
        </Fragment>
      ))}

      <Box
        sx={{
          "& > :not(style)": { m: 4, width: "25ch" },
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Button
          variant="contained"
          color="success"
          disabled={
            !consulta.sintomas[0] ||
            !consulta.sintomas[1] ||
            !consulta.sintomas[2]
          }
          onClick={() => {
            let dis = "";
            consulta.sintomas.sort().forEach((e) => (dis += `'${e}'`));
            session.query(
              `conocimiento(X, [${dis.split("''").join("','")}],Y).`,
              {
                success: function (goal) {
                  /* Goal loaded correctly */
                  session.answer({
                    success: function (answer) {
                      setState(session.format_answer(answer));
                      let res = session
                        .format_answer(answer)
                        .split("Y =")[1]
                        .trim()
                        .replace("[", "")
                        .replace("]", "")
                        .replace(".", "")
                        .split(",");
                      const arr = res;
                      console.log();
                      MySwal.fire({
                        icon: "warning",
                        title: JSON.stringify(
                          session
                            .format_answer(answer)
                            .split("X =")[1]
                            .split(",")[0]
                        ),
                        text: JSON.stringify(arr),
                      });
                    },
                    fail: function () {
                      setState("error");
                      MySwal.fire({
                        icon: "error",
                        title: "Error",
                        text: "No coincide los sintomas seleccionados con una enfermedad",
                      });
                      /* No more answers */
                    },
                    error: function (err) {
                      /* Uncaught exception */
                    },
                    limit: function () {
                      /* Limit exceeded */
                    },
                  });
                },
                error: function (err) {
                  /* Error parsing goal */
                },
              }
            );
          }}
        >
          Consultar
        </Button>
      </Box>
    </Container>
  );
}

export default App;
